# Webhook Pipeline

A simple application to launch a script using a GET HTTP call.

## Develop

```shell
npm init
```

## Start

```shell
npm start
```

## License

MIT License
