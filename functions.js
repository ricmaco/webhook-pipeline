const fs = require('node:fs');
const path = require('node:path');
const process = require('node:process');

const cmdLineArgs = require('command-line-args');
const pino = require('pino');

/** Parse command line flags and merge them to config */
function initConfig(defaultConfig) {
    const cmdLineArgsConfig = Object.keys(defaultConfig).map(k => ({
        name: k,
        alias: k.charAt(0),
        type: defaultConfig[k][1],
        defaultValue: defaultConfig[k][0]
    }));

    return cmdLineArgs(cmdLineArgsConfig);
}

/** Initialize logging library and paths */
function initLogging(logPath) {
    const transport = pino.transport({
        targets: [
            {
                target: 'pino/file'
            },
            {
                target: 'pino/file',
                options: {
                    destination: logPath,
                    mkdir: true
                }
            }
        ]
    });

    transport.on('error', (e) => {
        console.error(`unable to access ${logPath}: ${e}`);
        process.exit(1);
    });

    return pino(transport);
}

/** End response with the provided status code */
function endRes(res, status) {
    res.statusCode = status
    res.end();
}

/** Get token (/xxxx/thisone) from the provided url path */
function getToken(url) {
    const lastSlashIndex = url.lastIndexOf('/');
    if (lastSlashIndex < 0) return;

    const token = url.substring(lastSlashIndex);
    if (!token) return;

    return token.replace('/', '');
}

/** Search for a file named as requested in the provided directory */
function searchInDirectory(dir, name) {
    try {
        return fs
            .readdirSync(dir, { withFileTypes: true })
            .filter(item => !item.isDirectory())
            .filter(item => item.name.startsWith(name))
            .map(item => path.resolve(dir, item.name))
            [0];
    } catch (e) {
        console.error(e);
        return;
    }
}

/** Returns a formatted log message with timestamp, level and id */
function log(id, level, message) {
    const ts = new Date().toISOString();
    return `[${ts}][${id}][${level}] ${message}`
}

module.exports = {
    initConfig,
    initLogging,
    endRes,
    getToken,
    searchInDirectory,
    log
};
