const http = require('http');
const { spawn } = require('node:child_process');

const packageJson = require('./package.json');
const functions = require('./functions');

const DEFAULT_CONFIG = {
    port: [3000, Number],
    method: ['GET', String],
    url: ['/trigger', String],
    root: ['.', String],
    shell: ['/usr/bin/bash', String],
    logs: ['/var/log/webhook-pipeline.log', String]
}

const config = functions.initConfig(DEFAULT_CONFIG);
const log = functions.initLogging(config.logs);

function _log(stream, data) {
    if (!data) return;

    const output = `${data}`.trimEnd().split('\n');
    log.info({ token, [stream]: output });
}

const server = http.createServer((req, res) => {
    if (req.method !== config.method) {
        return functions.endRes(res, 405);
    }
    
    if (!req.url.startsWith(config.url)) {
        return functions.endRes(res, 404);
    }

    const token = functions.getToken(req.url);
    if (!token) {
        return functions.endRes(res, 400);
    }

    log.info(`${config.method} ${req.url}`);
    const script = functions.searchInDirectory(config.root, token);
    if (!script) {
        log.error(`${token} not found in ${config.root}`);
        return functions.endRes(res, 500);
    }

    log.info(`Spawning ${config.shell} ${script}`);
    const pipeline = spawn(config.shell, [script]);

    pipeline.stdout.on('data', (data) => {
        if (!data) return;

        const output = `${data}`.trimEnd().split('\n');
        log.info({ token, stdout: output });
    });
    pipeline.stderr.on('data', (data) => {
        if (!data) return;

        const output = `${data}`.trimEnd().split('\n');
        log.info({ token, stderr: output });
    });

    pipeline.on('close', (code) => {
        log.info({ token, msg: `exited with code ${code}` });
    });

    pipeline.on('error', (e) => {
        log.error({ token, exception: e });
        return functions.endRes(res, 500);
    });

    res.end();
});

server.listen(config.port, () => {
    log.info(`${packageJson.name} v${packageJson.version}`);
    log.info(`Started on port ${config.port}`)
})
